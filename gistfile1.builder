Then /^I should see that the name of the "([^"]*)" project is "([^"]*)"$/ do |number, name|
 locate(:xpath, "//ul[@id='project_summary']/li[#{number.to_i}]//a[@class='project']").should have_content(name)
end
